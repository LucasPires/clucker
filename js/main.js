import { Cu } from "./cu/cu.js";

const cu = new Cu();

function startUpdateInterval() {
    window.setInterval(update, 10);
}

function loading(){
    window.addEventListener("load",function(){
        var carregando = document.getElementById("carregando");
        document.body.removeChild(carregando);
    });
}

function update() {
    cu.updateTotalFinalPerTick();
}

function initialHide() {
    $("#msg2").hide();
    $("#itensLoja").hide();
    $("#esconde2").hide();
}

function prepareButtons() {
    $("#apertaCu").on("click", function () {
        cu.click();
    });
    $("#comerCu").on("click", function () {
        cu.comerCus();
    });
    $("#unlockShop").on("click", function () {
        cu.unlockShop();
    });
    $("#unlockUpgradeShop").on("click", function () {
        cu.unlockUpgradeShop();
    });
    $("#item1").on("click", function () {
        cu.buyItem(cu.shop.items.item1, ['valor1', 'quantidade1']);
    });
    $("#item2").on("click", function () {
        cu.buyItem(cu.shop.items.item2, ['valor2', 'quantidade2']);
    });
    $("#item3").on("click", function () {
        cu.buyItem(cu.shop.items.item3, ['valor3', 'quantidade3']);
    });
    $("#upgrade1").on("click", function () {
        cu.buyUpgrade(cu.shop.items.item1, 1, 1000);
    });
    $("#upgrade2").on("click", function () {
        cu.buyUpgrade(cu.shop.items.item2, 2, 10000);
    });
    $("#upgrade3").on("click", function () {
        cu.buyUpgrade(cu.shop.items.item3, 3, 100000);
    });
}

$(document).ready(function () {
    loading();
    prepareButtons();
    initialHide();
    startUpdateInterval();
});
