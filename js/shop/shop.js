import { Item } from "./item.js";
export class Shop {

    constructor() {
        this.items = {
            item1: new Item("Cuzinho virtual (feito de camisinha)", 1, 12),
            item2: new Item("Cu de borracha", 3, 30),
            item3: new Item("Curió na gaiola", 5, 50)
        };
    }

    buyItem(item) {
        item.quantity++;
        item.price = Math.ceil(item.price * 1.09);
    }
    
    buyUpgrade(item) {
        item.multiplier += 1;
    }
}