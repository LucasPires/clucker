export class Item {
    constructor(name, cps, price) {
        this.name = name;
        this.quantity = 0;
        this.cps = cps;
        this.multiplier = 1;
        this.price = price;
    }
}