import { Shop } from "../shop/shop.js";
export class Cu {
    constructor() {
        this.shop = new Shop();
        this.totalFinal = 0; //total de cuzitos (inicial de 0)
        this.cps = 0; //cuzitos por segundo (inicial de 0)
        this.comidos = 0; //cuzitos comidos
        this.cpc = 1; //cuzitos por clique (inicial de 0)
        this.cuMemory = 0; //total de cus apertados desde o inicio do jogo
    }

    updateCps() {
        this.cps = 0;
        Object.values(this.shop.items).forEach(item => {
            this.cps += item.cps * item.quantity * item.multiplier;
        });

        $('#csegundo').html(this.cps);
    }

    updateTotalFinalPerTick() {
        this.totalFinal += this.cps / 100;
        this.cuMemory += this.cps / 100;
        this.updateTotalFinal();
    }

    updateTotalFinal() {
        $("#total").html(Math.floor(this.totalFinal));
        document.title = Math.floor(this.totalFinal) + ' - Clucker';
    }

    click() {
        this.totalFinal += this.cpc;
        this.cuMemory += this.cpc;
        this.updateTotalFinal();
    }

    comerCus() {
        this.comidos += Math.floor(this.totalFinal);
        this.totalFinal = 0;
        $('#comidos').html(this.comidos);
        this.updateTotalFinal();
    };

    buyItem(item, elements) {
        if (this.totalFinal >= item.price) {
            this.totalFinal -= item.price;
            this.shop.buyItem(item);
            this.updateCps();
            this.updateShopDOM(item, elements);
            this.updateTotalFinal();

        } else {
            alert("vc n tem uma quantidade exata de cuzito...");
        }
    }

    buyUpgrade(item, elementNum, price) {
        if (this.totalFinal >= price) {
            this.totalFinal -= price;
            this.shop.buyUpgrade(item);
            this.updateCps();
            this.updateUpgradeDOM(elementNum, item);
        } else {
            alert("vc n tem uma quantidade exata de cuzito...");
        }
    }

    updateShopDOM(item, elements) {
        $('#' + elements[0]).html(item.price + ",00");
        $('#' + elements[1]).html(item.quantity);
    }

    updateUpgradeDOM(elementNum, item) {
        $('#upgradeText' + elementNum).hide();
        $('#cps'+elementNum).html(item.cps * item.multiplier);
    }

    unlockShop() {
        if (this.totalFinal >= 20 && this.comidos >= 20) {
            $("#msg").hide();
            $("#msg2").show();
            $("#itensLoja").show();
        } else {
            alert("voce ainda nao comeu ou nao apertou cu suficiente");
        }
    }

    unlockUpgradeShop() {
        if (this.totalFinal >= 500 && this.comidos >= 500) {
            $("#msg2").hide();
            $("#esconde2").show();
        } else {
            alert("voce ainda nao comeu ou nao apertou cu suficiente");
        }
    }

}